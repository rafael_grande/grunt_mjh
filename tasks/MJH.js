/*
 * grunt-mjh
 *
 * Copyright (c) 2015 MJH
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

      // Print a success message.
    var js_files = '';
    var images_files ='';
    var fonts_files = '';
    var js = '';
    var css = '';
    var html = '';
    var titulo = '';
    var index = '';
    
    
    
    grunt.registerTask('MJH', 'Concateca os js e os css na index.', function() {
        //Copiando imagens
        images_files = grunt.file.expand('assets/img/*.png, assets/img/*.jpg, assets/img/*/*.jpg');
        images_files.forEach(
            function (i){
                grunt.file.copy(i, '../dist/'+i);
            }
        );
        
        //Copiando imagens
        fonts_files = grunt.file.expand('css/fonts/*');
        fonts_files.forEach(
            function (fo){
                grunt.file.copy(fo, '../dist/'+fo);
            }
        );
        //inserindo as bibiotecas
        js = js + grunt.file.read('js/libs/jquery.min.js');
        js = js + grunt.file.read('js/libs/tweenmax.min.js');
        js = js + grunt.file.read('js/libs/sativa.min.js');
        //Inseri os arquivos js da pasta screen
        js_files = grunt.file.expand('js/**/*.js');
        js_files.forEach(
            function (f){
               js = js + grunt.file.read(f);
            }
        );
        
        /*Inseri js com as functions (main, script, app ...)*/
        js_files = grunt.file.expand('js/*.js');
        js_files.forEach(
            function (f){
               js = js + grunt.file.read(f);
            }
        );
        
        js = js.split('Main.getInstance()').join('project.areas.Main.getInstance()');
        js = js.split('Navigate.getInstance()').join('sativa.navigate.Navigate.getInstance()');
        
        
        //Recebe todo codigo do css
        css =  grunt.file.read('css/screen.css');
        //css = css.replaceAll('..assets','assets');
        
        css = css.split('../assets').join('assets');
        css = css.split('../fonts').join('css/fonts');
        css = css.split('./fonts').join('css/fonts');
        //Recebe todo o codigo do arquivo
        html = grunt.file.read('index.html');
        // Pega so o titulo do codigo
        titulo = html.substring(html.indexOf('<title>') + 7,html.indexOf('</title>'));
        //pega o conteudo do body
        html = html.substring(html.indexOf('<body>') + 6,html.indexOf('</body>'));
        //coloca tudo dntro da variavel index
        index = '<html>'+
                    '<head>'+
	                   '<meta charset="UTF-8">'+
	                   '<title>'+titulo+'</title>'+
                        '<style>+'+css+'</style>'+
                      '</head>'+
                    '<body>'+html+
                        '<script>'+js+'</script>'+
                    '</body>'+
                '</html>';
        
        
        //Cria o arquivo index html na pasta dest
        grunt.file.write('../dist/index.html', index);
        
        console.log("Bora colocar moldura!!!");
        
    });
      

};